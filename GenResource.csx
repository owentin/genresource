using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;
using System.Text.RegularExpressions;

public class ResourceData
{
    public string Name {get; set;}
    public string Value {get; set;}
}

string currentPath = System.IO.Directory.GetCurrentDirectory();

string sourceFile = $"{currentPath}\\resource.csv";
if (!File.Exists(sourceFile)) 
{
    throw new Exception($"{sourceFile} is not exist");
}

var destFile = $"{currentPath}\\output.resx";

Console.WriteLine($"Will start to generate resource file by csv");
if (File.Exists(destFile)) 
{
    Console.WriteLine($"{destFile} is exist and will be deleted");
}
Console.WriteLine("");
Console.WriteLine($"Press any key to continue or Ctrl+C to abort...");
Console.Read();

if (File.Exists(destFile)) 
{
    File.Delete(destFile);
}

var reader = new StreamReader(File.OpenRead(sourceFile));
List<ResourceData> sources = new List<ResourceData>();
while (!reader.EndOfStream)
{
    var item = reader.ReadLine();
    var data = Regex.Split(item, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

    sources.Add(new ResourceData 
    {
        Name = data[0],
        Value = data[1].TrimStart('"').TrimEnd('"')
    });
}

Dictionary<string, int> duplicates = sources.GroupBy(x => x.Name).ToDictionary(g => g.Key,
                                                                               g => g.Count());
if (duplicates.Any() && duplicates.Where(d => d.Value > 1).Count() > 0) 
{
    foreach (var item in duplicates) 
    {
        Console.WriteLine($"{item.Key} repeat {item.Value} times");
    }
    throw new Exception($"Duplicate item found");
}

using (ResXResourceWriter resx = new ResXResourceWriter(destFile))
{
    foreach (var source in sources) 
    {
        resx.AddResource(source.Name, source.Value);
    }
}